# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
admin = User.first_or_create!(email: Rails.application.credentials.admin[:username],
                              password: Rails.application.credentials.admin[:password],
                              password_confirmation: Rails.application.credentials.admin[:password])

Course.all.each do |course|
  course.user_id ||= admin.id
  course.save
end
