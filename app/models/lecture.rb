class Lecture < ApplicationRecord
  belongs_to :course
  has_one_attached :video

  validates :title, :course, presence: true
  validates :title, length: { maximum: 80 }
  validates_uniqueness_of :title, scope: :course_id

  validates :video,
            content_type: ['video/mp4', 'video/quicktime'],
            size: { less_than: 500.megabytes, message: 'size should be under 500 megabytes' }

  def to_s
    title
  end
end
