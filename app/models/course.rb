class Course < ApplicationRecord
  has_many :lectures, dependent: :destroy
  belongs_to :user

  validates :title, :user, presence: true
  validates_uniqueness_of :title

  def to_s
    title
  end
end
