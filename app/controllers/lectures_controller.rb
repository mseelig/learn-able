class LecturesController < ApplicationController
  before_action :set_course
  before_action :set_lecture, only: %i[show edit update destroy delete_video]

  # GET /lectures or /lectures.json
  def index
    @lectures = @course.lectures
  end

  # GET /lectures/1 or /lectures/1.json
  def show; end

  # GET /lectures/new
  def new
    @lecture = @course.lectures.build
  end

  # GET /lectures/1/edit
  def edit; end

  # rubocop:disable Metrics/MethodLength
  # POST /lectures or /lectures.json
  def create
    @lecture = @course.lectures.build(lecture_params)

    respond_to do |format|
      if @lecture.save
        format.html do
          redirect_to course_lectures_path(@course),
                      notice: "Lecture \"#{@lecture.title}\" was successfully created."
        end
        format.json { render :show, status: :created, location: @lecture }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @lecture.errors, status: :unprocessable_entity }
      end
    end
  end
  # rubocop:enable Metrics/MethodLength

  # rubocop:disable Metrics/MethodLength
  # PATCH/PUT /lectures/1 or /lectures/1.json
  def update
    respond_to do |format|
      if @lecture.update(lecture_params)
        format.html do
          redirect_to course_lectures_path(@course),
                      notice: "Lecture \"#{@lecture.title}\" was successfully updated."
        end
        format.json { render :show, status: :ok, location: @lecture }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @lecture.errors, status: :unprocessable_entity }
      end
    end
  end
  # rubocop:enable Metrics/MethodLength

  # DELETE /lectures/1 or /lectures/1.json
  def destroy
    @lecture.destroy
    respond_to do |format|
      format.html do
        redirect_to course_lectures_path(@course),
                    notice: "Lecture \"#{@lecture.title}\" was successfully removed."
      end
      format.json { head :no_content }
    end
  end

  def delete_video
    @lecture.video.purge
    redirect_to edit_course_lecture_path(@course, @lecture), notice: 'Video successfully deleted!'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_course
    @course = Course.find(params[:course_id])
  end

  def set_lecture
    @lecture = @course.lectures.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def lecture_params
    params.require(:lecture).permit(:title, :course_id, :video)
  end
end
