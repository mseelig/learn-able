Rails.application.routes.draw do
  devise_for :users
  root 'welcome#index'

  resources :courses do
    resources :lectures do
      member do
        delete :delete_video
      end
    end
    member do
      get :preview
    end
  end

  get 'welcome/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
