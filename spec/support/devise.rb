require_relative './controller_macros'

RSpec.configure do |config|
  config.extend ControllerMacros, type: :request
  config.include Devise::Test::IntegrationHelpers, type: :request
  config.include Devise::Test::ControllerHelpers, type: :view
end
