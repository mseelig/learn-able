FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "test-#{n}@sample.com" }
    password { '123456' }
  end

  factory :course do
    sequence(:title) { |n| "course-#{n}" }
    association :user, factory: :user
  end

  factory :lecture do
    sequence(:title) { |n| "lecture-#{n}" }
    association :course, factory: :course
  end
end
