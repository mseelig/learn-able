require 'rails_helper'
require_relative '../../support/devise'

RSpec.describe 'lectures/show', type: :view do
  before(:each) do
    @course = assign(:course, FactoryBot.create(:course))
    @lecture = assign(:lesson, FactoryBot.create(:lecture, course: @course))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(//)
  end
end
