require 'rails_helper'
require_relative '../../support/devise'

RSpec.describe 'lectures/index', type: :view do
  before(:each) do
    @course = assign(:course, FactoryBot.create(:course))
    assign(:lectures,
           [
             FactoryBot.create(:lecture, course: @course),
             FactoryBot.create(:lecture, course: @course)
           ])
  end

  it 'renders a list of lectures' do
    render
    assert_select 'div.row', text: /Lecture/, count: 2
  end
end
