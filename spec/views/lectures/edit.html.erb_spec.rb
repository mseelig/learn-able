require 'rails_helper'
require_relative '../../support/devise'

RSpec.describe 'lectures/edit', type: :view do
  before(:each) do
    @course = assign(:course, FactoryBot.create(:course))
    @lecture = assign(:lesson, FactoryBot.create(:lecture, course: @course))
  end

  it 'renders the edit lecture form' do
    render

    assert_select 'form[action=?][method=?]', course_lecture_path(@course, @lecture), 'post' do
      assert_select 'input[name=?]', 'lecture[title]'
    end
  end
end
