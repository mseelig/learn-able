require 'rails_helper'
require_relative '../../support/devise'

RSpec.describe 'courses/edit', type: :view do
  before(:each) do
    @course = assign(:course, FactoryBot.create(:course))
  end

  it 'renders the edit course form' do
    render

    assert_select 'form[action=?][method=?]', course_path(@course), 'post' do
      assert_select 'input[name=?]', 'course[title]'
    end
  end
end
