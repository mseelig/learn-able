require 'rails_helper'
require_relative '../../support/devise'

RSpec.describe 'courses/new', type: :view do
  before(:each) do
    @course = assign(:course, FactoryBot.build(:course))
  end

  it 'renders new course form' do
    render

    assert_select 'form[action=?][method=?]', courses_path, 'post' do
      assert_select 'input[name=?]', 'course[title]'
    end
  end
end
