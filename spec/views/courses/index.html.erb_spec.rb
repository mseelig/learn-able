require 'rails_helper'
require_relative '../../support/devise'

RSpec.describe 'courses/index', type: :view do
  before(:each) do
    assign(:courses,
           [FactoryBot.create(:course),
            FactoryBot.create(:course)])
  end

  it 'renders a list of courses' do
    render

    assert_select 'div.row', text: /Course/, count: 2
  end
end
