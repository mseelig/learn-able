require 'rails_helper'
require_relative '../../support/devise'

RSpec.describe 'courses/preview', type: :view do
  before(:each) do
    @course = assign(:course, FactoryBot.create(:course))
    @lecture = assign(:lecture, FactoryBot.create(:lecture, course: @course))
  end

  it 'renders attributes on page' do
    render
    skip('Add matchers for preview')
    expect(rendered).to match(/@course/)
    expect(rendered).to match(/@lecture/)
  end
end
