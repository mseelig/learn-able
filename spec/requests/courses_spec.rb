require 'rails_helper'
require_relative '../support/devise'

RSpec.describe '/courses', type: :request do
  let(:valid_attributes) do
    {
      title: 'My Course',
      user: FactoryBot.create(:user)
    }
  end

  let(:invalid_attributes) do
    {
      title: nil
    }
  end

  describe 'GET /index' do
    login_user
    it 'renders a successful response' do
      FactoryBot.create(:course)
      get courses_url
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    login_user
    it 'renders a successful response' do
      course = FactoryBot.create(:course)
      get course_url(course)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    login_user
    it 'renders a successful response' do
      get new_course_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    login_user
    it 'render a successful response' do
      course = FactoryBot.create(:course)
      get edit_course_url(course)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    login_user
    context 'with valid parameters' do
      it 'creates a new Course' do
        expect do
          post courses_url, params: { course: valid_attributes }
        end.to change(Course, :count).by(1)
      end

      it 'redirects to the courses index' do
        post courses_url, params: { course: valid_attributes }
        expect(response).to redirect_to(courses_url)
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Course' do
        expect do
          post courses_url, params: { course: invalid_attributes }
        end.to change(Course, :count).by(0)
      end

      it 'returns a 422 Unprocessable Entity response' do
        post courses_url, params: { course: invalid_attributes }
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'PATCH /update' do
    login_user
    context 'with valid parameters' do
      let(:new_attributes) do
        {
          title: 'New Title'
        }
      end

      it 'updates the requested course' do
        course = FactoryBot.create(:course)
        patch course_url(course), params: { course: new_attributes }
        course.reload
        skip('Add assertions for updated state')
      end

      it 'redirects to the courses index' do
        course = FactoryBot.create(:course)
        patch course_url(course), params: { course: new_attributes }
        course.reload
        expect(response).to redirect_to(courses_url)
      end
    end

    context 'with invalid parameters' do
      it 'returns a 422 Unprocessable Entity response' do
        course = FactoryBot.create(:course)
        patch course_url(course), params: { course: invalid_attributes }
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'DELETE /destroy' do
    login_user
    it 'destroys the requested course' do
      course = FactoryBot.create(:course)
      expect do
        delete course_url(course)
      end.to change(Course, :count).by(-1)
    end

    it 'redirects to the courses list' do
      course = FactoryBot.create(:course)
      delete course_url(course)
      expect(response).to redirect_to(courses_url)
    end
  end
end
