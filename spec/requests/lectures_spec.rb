require 'rails_helper'
require_relative '../support/devise'

RSpec.describe '/lectures', type: :request do
  let(:valid_attributes) do
    {
      title: 'My Lecture',
      course: FactoryBot.create(:course)
    }
  end

  let(:invalid_attributes) do
    {
      title: nil
    }
  end

  let(:course) do
    FactoryBot.create(:course)
  end

  let(:lecture) do
    FactoryBot.create(:lecture, course: course)
  end

  describe 'GET /index' do
    login_user
    it 'renders a successful response' do
      get course_lectures_url(course)
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    login_user
    it 'renders a successful response' do
      get course_lecture_url(course, lecture)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    login_user
    it 'renders a successful response' do
      get new_course_lecture_url(course)
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    login_user
    it 'render a successful response' do
      get edit_course_lecture_url(course, lecture)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    login_user
    context 'with valid parameters' do
      it 'creates a new Lecture' do
        expect do
          post course_lectures_url(course), params: { lecture: valid_attributes }
        end.to change(Lecture, :count).by(1)
      end

      it 'redirects to the course lectures' do
        post course_lectures_url(course), params: { lecture: valid_attributes }
        expect(response).to redirect_to(course_lectures_url(course))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Lecture' do
        expect do
          post course_lectures_url(course), params: { lecture: invalid_attributes }
        end.to change(Lecture, :count).by(0)
      end

      it 'returns a 422 Unprocessable Entity response' do
        post course_lectures_url(course), params: { lecture: invalid_attributes }
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'PATCH /update' do
    login_user
    context 'with valid parameters' do
      let(:new_attributes) do
        {
          title: 'New Title'
        }
      end

      it 'updates the requested lecture' do
        patch course_lecture_url(course, lecture), params: { lecture: new_attributes }
        lecture.reload
        skip('Add assertions for updated state')
      end

      it 'redirects to the course lectures' do
        patch course_lecture_path(course, lecture), params: { lecture: new_attributes }
        lecture.reload
        expect(response).to redirect_to(course_lectures_url(course))
      end
    end

    context 'with invalid parameters' do
      it 'returns a 422 Unprocessable Entity response' do
        patch course_lecture_url(course, lecture), params: { lecture: invalid_attributes }
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'DELETE /destroy' do
    login_user
    it 'destroys the requested lecture' do
      lecture.reload
      expect do
        delete course_lecture_url(course, lecture)
      end.to change(Lecture, :count).by(-1)
    end

    it 'redirects to the course lectures list' do
      delete course_lecture_url(course, lecture)
      expect(response).to redirect_to(course_lectures_url(course))
    end
  end
end
